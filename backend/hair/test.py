import cv2
from matplotlib import pyplot as plt

path = '/home/alenorze/Workspace/Current/hair/backend/test_data/0e6303c4-0b29-457e-ab74-ac2d1cfb1ecf.jpeg'
maxwidth = 650
height = 500
# cs = cv2.pythoncuda.gpuResize(path, maxwidth, height)

cvd = cv2.gpuResize(path, maxwidth, height)
plt.imshow(cvd)
plt.show()