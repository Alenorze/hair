import pickle
import os
import glob
import json
import shutil
import json

import jwt
from datetime import datetime, timedelta
from functools import wraps
from timeit import default_timer as timer
from uuid import uuid4
from pathlib import Path

from PIL import Image
from celery.result import AsyncResult
from .tasks import app as celery_app
from flask import Flask, request, abort, Blueprint, jsonify, current_app, send_from_directory
from werkzeug.exceptions import HTTPException
from flask_cors import CORS
from redis import Redis

from utils.colors import get_colors
from utils.misc import send_image
from utils.reinhard import collage
from .models import db, User
from .config import DATA_DIR, ID_KEY, COLOR_KEY, MAXWIDTH, EXPIRES, JPEG_QUALITY, REDIS_HOST
from .tasks import create_mask


r = Redis(host=REDIS_HOST, port=6379, db=0)
api = Blueprint('api', __name__)
render = Blueprint('render', __name__)

def token_required(f):
    @wraps(f)
    def _verify(*args, **kwargs):
        auth_headers = request.headers.get('Authorization', '').split()

        invalid_msg = {
            'message': 'Invalid token. Authentication required.',
            'authenticated': False
        }
        expired_msg = {
            'message': 'Expired token. Reauthentication required.',
            'authenticated': False
        }

        if request.args.get('token'):
            token = request.args.get('token')
        elif len(auth_headers) == 2:
            token = auth_headers[1]
        else:
            return jsonify(invalid_msg), 401

        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'])

            user = User.query.filter_by(username=data['sub']).first()
            if not user:
                raise RuntimeError('User not found')

            return f(user, *args, **kwargs)
    
        except jwt.ExpiredSignatureError:
            return jsonify(expired_msg), 401
        except jwt.InvalidTokenError:
            return jsonify(invalid_msg), 401
        except HTTPException as e:
            return e

    return _verify


@render.route('/')
def index():
    return send_from_directory('./', 'index.html')


@api.route('/photo', methods=['POST', 'GET'])
@token_required
def upload(self):
    if request.method == 'POST':
        if 'photo' not in request.files:
            return abort(400, description='There isn\'t photo field')

        photo = request.files['photo']
        photo_path = Path(DATA_DIR) / Path(str(uuid4())
                                           ).with_suffix(Path(photo.filename).suffix)
        path = str(photo_path.absolute())
        photo.save(path)

        task = create_mask.delay(path)

        return task.id

    try:
        photo_id = request.args[ID_KEY]
        params = request.args[COLOR_KEY]

    except KeyError:
        return abort(400, description='photo_id or params didn\'t set')

    res = celery_app.AsyncResult(photo_id)

    if res.state == 'PENDING':
        return abort(404, description='Image in process')
    elif res.state == 'FAILURE':
        return abort(500, description='Task failed')

    obj = r.lrange(photo_id, 0, 0)
    if len(obj) == 0:
        return abort(404, description='There is not a task with this id')
    obj = pickle.loads(obj[0])

    try:
        means, stds = get_colors(params)
    except RuntimeError as err:
        return abort(400, description=str(err))

    for mean, std in zip(means, stds):
        if mean is None or std is None:
            continue
        if mean.shape != (3,) or std.shape != (3,):
            return abort(400, description='Shapes of mean and std must be equal to 3')
    
    try:
        image = collage(obj['image_rgba'], obj['image_lab'],
                        obj['mean'], obj['std'], means, stds, MAXWIDTH)
    except RuntimeError as err:
        return abort(400, description=str(err))

    send_raw = request.args.get('download', False)
    user_agent = request.user_agent.string.lower()
    safari = ('safari' in user_agent) and ('chrome' not in user_agent)

    if request.args.get('small'):
        quality = 1

    return send_image(image, quality=JPEG_QUALITY, raw=send_raw, safari=safari)


@api.route('/register/', methods=('POST',))
def register():
    data = request.get_json()
    user = User(**data)
    db.session.add(user)
    db.session.commit()
    return jsonify(user.to_dict()), 201


@api.route('/login/', methods=('POST',))
def login():
    data = request.get_json()
    user = User.authenticate(**data)

    if not user:
        return jsonify({'message': 'Invalid credentials', 'authenticated': False}), 401

    token = jwt.encode({
        'sub': user.username,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=30)},
        current_app.config['SECRET_KEY'])
        
    return jsonify({'token': token.decode('UTF-8')})


@api.route('/highload/', methods=['GET', 'POST'])
@token_required
def highload(self):
    for filepath in Path('test_data').glob('**/*'):
        path = str(filepath.absolute())
        create_mask.delay(path)

    return 'OK'


@api.route('/results/', methods=['GET', 'POST'])
@token_required
def results(self):
    count = 0
    l = None
    total_time = 'No time'

    if request.args.get('time'):
        time = request.args.get('time')
    else:
        time = 'Not used'

    if request.args.get('action') == 'clear':
        with open('results.json') as f:
            l = json.load(f)
            l = []
        with open('results.json', 'w') as f:
            json.dump(l, f)

        return jsonify({'time': time, 'count': count, 'total_time': total_time, 'results': l})

    if request.method == 'GET':

        with open('results.json') as f:
            l = json.load(f)

        num = 0
        count = len(l)
        total_time = sum(x['time'] for x in l)
        for x in l:
            num += 1
            x.update({"num": num})

    return jsonify({'time': time, 'count': count, 'total_time': total_time, 'results': l})


if __name__ == '__main__':
    app.run()

