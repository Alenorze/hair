import json
from pathlib import Path

import numpy as np



with open(str(Path(__file__).parents[1] / 'colors' / 'colors.json')) as f:
    colors_db = json.load(f)


def get_color_from_db(key):
    data = colors_db[key.upper()]
    return data['mean'], data['std']


def get_single_color(color):
    if isinstance(color, str):
        if color == "":
            return None, None
        return get_color_from_db(color)
    elif isinstance(color, dict):
        return color['mean'], color['std']
    else:
        raise RuntimeError('Wrong color format')


def to_arrays(arr):
    return [np.array(x) if x is not None else None for x in arr]


def get_colors(params):
    try:
        color = json.loads(params)
        if isinstance(color, (str, dict)):
            data = get_single_color(color)
            means = [data[0]]
            stds = [data[1]]
        elif isinstance(color, list):
            data = [get_single_color(c) for c in color]
            means = [d[0] for d in data]
            stds = [d[1] for d in data]
        else:
            raise RuntimeError(
                'You must pass list or dict as color information')
        means = to_arrays(means)
        stds = to_arrays(stds)
    except (json.JSONDecodeError, KeyError, TypeError):
        raise RuntimeError('Cannot extract color information')
    return means, stds
