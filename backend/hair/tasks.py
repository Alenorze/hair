import os
import json
import pickle
import cv2

from time import time

import tensorflow as tf
from celery import Celery
from celery.signals import worker_process_init, task_prerun, task_postrun
from celery.utils.log import get_logger
from redis import Redis

from .config import EXPIRES, MODEL_FILENAME, RABBITMQ_URL, REDIS_HOST, REDIS_URL, GF_RADIUS, GF_EPS

os.environ['CUDA_VISIBLE_DEVICES'] = '0' 
sess = None
r = None
app = Celery('tasks', backend=REDIS_URL, broker=RABBITMQ_URL)
app.conf.update(result_expires=EXPIRES)
logger = get_logger(__name__)



@worker_process_init.connect()
def init_worker(**kwargs):
    global sess, r
    with tf.io.gfile.GFile(MODEL_FILENAME, 'rb') as f:
        graph_def = tf.compat.v1.GraphDef()
        graph_def.ParseFromString(f.read())
        tf.import_graph_def(graph_def)
        
    gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.2, allow_growth=True)
    sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(gpu_options=gpu_options))
    r = Redis(host=REDIS_HOST, port=6379, db=0)


@app.task()
def create_mask(path):
    
    maxwidth = 500

    try:
        image = cv2.gpuResize(path, maxwidth) / 255.0

        img_rgba = sess.run(
            sess.graph.get_tensor_by_name('import/outputs/hair:0'),
            feed_dict={'import/inputs/images:0': [image]})
        img_rgba = img_rgba[0]

        image, image_mask = img_rgba[..., :3], img_rgba[..., 3]

        rect = cv2.gpuFindSides(image_mask)
        x = rect[0][0]
        y = rect[1][0]
        w = rect[2][0]
        h = rect[3][0]

        img_lab = cv2.gpuLab(image[y:y+h, x:x+w])
        img_rgba = img_rgba[y:y+h, x:x+w]
        img_mean, img_std = cv2.meanStdDev(img_lab)
        img_mean = img_mean.reshape((3,))
        img_std = img_std.reshape((3,))

        img_rgba[..., -1] = cv2.ximgproc.guidedFilter(img_rgba[..., :-1],
                                            img_rgba[..., -1:], GF_RADIUS, GF_EPS)
        
    except ValueError:
        raise ValueError('Cannot identify image file')
    
    r.lpush(create_mask.request.id, pickle.dumps({
        'image_rgba': img_rgba,
        'image_lab': img_lab,
        'mean': img_mean,
        'std': img_std
    }))
    r.expire(create_mask.request.id, EXPIRES)
    

# Signals for results
d = {}

@task_prerun.connect
def task_prerun_handler(signal, sender, task_id, task, args, kwargs, **extras):
    d[task_id] = time()


@task_postrun.connect
def task_postrun_handler(signal, sender, task_id, task, args, kwargs, retval, state, **extras):
    try:
        cost = time() - d.pop(task_id)
    except KeyError:
        cost = -1

    cost = {'id': task_id, 'time': cost, 'state': state, 'error': str(retval)}

    with open('results.json') as f:
        l = json.load(f)
        l.append(cost)

    with open('results.json', 'w') as f:    
        json.dump(l, f)

