from flask import Flask
from flask_cors import CORS

from hair.api import api, render
from hair.models import db



def create_app(app_name='HAIR'):
    app = Flask(app_name)
    app.config.from_object('hair.config.BaseConfig')
    cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

    app.register_blueprint(api, url_prefix="/api")
    app.register_blueprint(render, url_prefix="/")
    
    db.init_app(app)

    return app
