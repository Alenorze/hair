// store/index.js

import Vue from 'vue'
import Vuex from 'vuex'

import { authenticate, register } from '@/api'
import { isValidJwt, EventBus } from '@/utils'

Vue.use(Vuex)

const state = {
  jwt: ''
}

const actions = {
  login (context, userData) {
    context.commit('setUserData', { userData })
    return authenticate(userData)
      .then(response => context.commit('setJwtToken', { jwt: response.data }))
      .catch(error => {
        console.log('Error Authenticating: ', error)
        EventBus.emit('failedAuthentication', error)
      })
  },
  register (context, userData) {
    context.commit('setUserData', { userData })
    return register(userData)
      .then(context.dispatch('login', userData))
      .catch(error => {
        console.log('Error Registering: ', error)
        EventBus.emit('failedRegistering: ', error)
      })
  },
}

const mutations = {
  setUserData (state, payload) {
    state.userData = payload.userData
  },
  setJwtToken (state, payload) {
    localStorage.token = payload.jwt.token
  }
}

const getters = {
  isAuthenticated () {
    console.log(localStorage.token)
    return isValidJwt(localStorage.token)
  }
}

const store = new Vuex.Store({
  state,
  actions,
  mutations,
  getters
})

export default store
