class BaseConfig(object):
    DEBUG = True
    SECRET_KEY = '414d0dd3-587d-4c03-baca-82fb1b91a101' 
    SQLALCHEMY_DATABASE_URI = 'sqlite:///sqlite.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


ID_KEY = 'id'
COLOR_KEY = 'params'
GF_RADIUS = 15
GF_EPS = 1e-6
MAXWIDTH = 500
JPEG_QUALITY = 100
DOCKER = False

if DOCKER:
    REDIS_HOST = 'redis'
    REDIS_URL = 'redis://redis:6379/0'
    RABBITMQ_URL = 'pyamqp://guest@rabbitmq//'
else:
    REDIS_HOST = 'localhost'
    REDIS_URL = 'redis://localhost:6379/0'
    RABBITMQ_URL = 'pyamqp://guest@localhost//'

DATA_DIR = './data'
MODEL_FILENAME = './models/Model1_1.pb'
EXPIRES = 1 * 60 * 60  # 1 hour
FACE_CROP = True
