import math

import cv2
import numpy as np

from skimage.transform import rescale

EPSILON = 1e-8
FIX_AB_STD = 1.0

ALPHA_CORR = 0.9
FIX_L_MEAN = 0.5
FIX_L_STD = 0.5


def color_transfer(image_out, image_with_alpha, image_lab, img_mean, img_std, mean, std):
    image, image_mask = image_with_alpha[..., :3], image_with_alpha[..., 3]

    np.copyto(image_out, image_lab)
    image_out -= img_mean.reshape((1, 1, 3))

    img_std_l = img_std[0]
    img_std_a = img_std[1]
    img_std_b = img_std[2]

    style_std_l = img_std_l + FIX_L_STD * (std[0] - img_std_l)
    style_std_a = std[1]
    style_std_b = std[2]

    m = np.zeros((3,))
    m[0] = style_std_l / (img_std_l + EPSILON)
    m[1] = style_std_a / (img_std_a + EPSILON) * FIX_AB_STD
    m[2] = style_std_b / (img_std_b + EPSILON) * FIX_AB_STD

    image_out *= m.reshape((1, 1, 3))

    mean[0] = img_mean[0] + FIX_L_MEAN * (mean[0] - img_mean[0])
    image_out += mean.reshape((1, 1, 3))

    cv2.cvtColor(image_out.astype(np.float32),
                 cv2.COLOR_LAB2RGB, dst=image_out)

    mask = image_mask[..., np.newaxis] * ALPHA_CORR
    mask = np.tile(mask, [1, 1, 3])
    image_out *= mask * ALPHA_CORR
    image_out += image * (1.0 - mask)
    return image_out


def collage(image_with_alpha, image_lab, img_mean, img_std, means, stds, maxwidth):
    if len(stds) != len(means):
        raise RuntimeError('len(means) != len(stds)')

    k = int(math.sqrt(len(means)))
    if k ** 2 > len(means):
        raise RuntimeError('size is too big for this means&stds')

    h, w, _ = image_with_alpha.shape

    if w * k > maxwidth:
        scale = maxwidth / w / k
        img_rgba = rescale(image_with_alpha, scale, mode='reflect',
                           multichannel=True, anti_aliasing=0.15)
        img_lab = rescale(image_lab / 255.0, scale, mode='reflect',
                          multichannel=True, anti_aliasing=0.15) * 255.0
    else:
        img_rgba = image_with_alpha
        img_lab = image_lab

    hs, ws, _ = img_rgba.shape
    img = np.zeros((hs * k, ws * k, 3), dtype=np.float32)
    for r in range(k):
        for c in range(k):
            mean = means[r * k + c]
            std = stds[r * k + c]
            if mean is None or std is None:
                img[r * hs:(r + 1) * hs, c * ws:(c + 1)
                    * ws, :] = img_rgba[..., :3]
            else:
                color_transfer(img[r * hs:(r + 1) * hs, c * ws:(c + 1) * ws, :],
                               img_rgba, img_lab, img_mean, img_std, mean, std)
    return img  

