import cv2
import numpy as np
import tensorflow as tf

from io import BytesIO
from flask import send_file
from imageio import imsave


def send_image(data, quality, raw=False, safari=False):
    filename = 'photo.jpeg'
    mimetype = 'image/jpeg'
    if raw and safari:
        mimetype = 'application/octet-stream'
    img_io = BytesIO()
    imsave(img_io, data, format='JPEG', quality=quality)
    img_io.seek(0)
    return send_file(img_io, mimetype=mimetype, as_attachment=raw, attachment_filename=filename)
