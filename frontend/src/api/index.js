
// api/index.js

import axios from 'axios'


export function authenticate (userData) {
  return axios.post(`/api/login/`, userData)
}

export function register (userData) {
  return axios.post(`/api/register/`, userData)
}

 