#!/usr/bin/env python3

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from hair.application import create_app
from hair.models import db

app = create_app()

migrate = Migrate(app, db)
manager = Manager(app)

# provide a migration utility command
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
